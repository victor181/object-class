## Quando usar um Objeto ou um Array?

Um Array pode ter vários formatos de dados em cada um de seus índices, mas isso não quer dizer que seja uma boa prática usá-los dessa maneira. Um array com vários tipos de dados misturados costuma ser confuso de entender, e dificulta em muito a correção de bugs.

Por exemplo, ao tentar usar um Array para armazenar o registro de um livro em estoque. Os dados pareceriam complexos e de diferentes tipos, além de tornar necessário o controle das índices da estrutura. Teriamos algo assim:

```javascript
let livroArr = [
    "Harry Potter",
    "Harry Potter e a Pedra Filosofal",
    208,
    15648977,
    "J.K Rowling"
]
```

Por que "Harry Potter" aparece duas vezes? O que são esses números? **O exemplo acima vai totalmente contra todas as boas práticas de desenvolvimento**. 

Nesses casos é muito mais efetivo usar a estrutura de um Objeto:

```javascript
let livroObj = {
    saga: "Harry Potter",
    volume: "Harry Potter e a Pedra Filosofal",
    paginas: 208,
    codigo: 15648977,
}
```

Agora sim, muito melhor.

~~Selo Victor de aprovação (MDN também :D)~~

Arrays são estruturas de dados comumente usadas para armazenar vários dados de mesmo tipo e para facilitar a iteração pelos seus itens. Por outro lado, objetos são estruturas para modelagem de dados comumente usadas para facilitar a organização de dados relacionados. Um Objeto __NÃO__ é iteravel.

## Acessar, criar ou alterar um atributo de um Objeto.

Dentro de um objeto, seus atributos funcionam como uma variável que pode ser acessada apenas por ele. Sendo assim, para acessar um atributo de um objeto, é necessário usar o "**.**" depois do nome de objeto:

```javascript
console.log(livroObj.saga)      //Retorna "Harry Potter"
console.log(livroObj.paginas)   //Retorna 208
```
Para alterar os dados desse atributo, também precisamos acessar o objeto primeiro. **Lembrando que assim como uma variável, esse valor pode assumir qualquer tipo e valor:**

```javascript
livroObj.codigo = 65487565;
//ou
livroObj.codigo = "65487565";
```

Para criar um novo atributo, novamente acessamos o nosso objeto, mas dessa vez, depois do "." colocaremos o nome do novo atributo:

```javascript
livroObj.autora = "J.K Rowling";
console.log(livroObj) //Retorna {saga: "Harry Potter", volume: "Harry Potter e a Pedra Filosofal", paginas: 208, codigo: 15648977, autora: "J.K Rowling"}
```

#### Atributos Computados?

Haverão vezes onde será necessário a criação dinâmica de Propriedades em um objeto, nesse caso, você poderá fazer uso de Atributos Computados. Para criar um atributo computado, você primeiro precisa chamar o objeto, e então colocar entre colchetes o nome da nova propriedade:

```javascript
livroObj["capitulo 1"] = "O menino que sobreviveu";
console.log(livroObj) //Retorna {saga: "Harry Potter", volume: "Harry Potter e a Pedra Filosofal", paginas: 208, codigo: 15648977, autora: "J.K Rowling", "capitulo um": "O menino que sobreviveu"}
```

Note que há um espaço entre a palavra e o número, sim, é possível criar atributos com nomes compostos, embora **não seja boa prática**.

> Ok, Victor, mas onde é usado essa sintaxe em um projeto real?

Quando for necessário criar vários cápitulos de uma vez, para evitar ficar escrevendo muuuito código e para se tornar um código escalável, você pode acabar precisando desse formato.

```javascript
let capitulos = [
    "O menino que sobreviveu",
    "O vidro que sumiu",
    "As cartas de ninguém",
    "O guardião das chaves",
    "O Beco Diagonal"
];

for (let contador = 0; contador < capitulos.length; contador++) {
    let novoAtributo = `capitulo ${contador + 1}`;

    livroObj[novoAtributo] = capitulos[contador];
}
```

Tá lá, criado vários outros capitulos do livro, facilmente. Eu aconselho vocês a tentarem algo parecido no seu console para aprender de verdade, ou copiem o código desde o inicio da declaração de **livroObj** e altere os valores para experimentar. :D


## Como funcionam as funções em Objetos?

Os atributos de um objeto podem receber como valor qualquer tipo de estrutura, incluindo __funções__. Dentro de um objeto, a função passa a ser chamada de __``Método``__. Não há mudanças complexas no funcionamento de funções, é apenas uma mudança de nomenclatura para podermos identificar facilmente que essa __função__ pertence a um __objeto__


Sendo assim, podemos declarar isto:

```javascript
let carro = {
    cor: "preto",
    portas: 4,
    ligar: function() {
        //código para ligar o carro
    },
    buzinar: () => {
        //código para buzinar
    }
}
```

E para chamar o método, novamente acessamos o objeto e com "." chamamos o método:

```Javascript
    carro.ligar() //Executará a função de ligar o carro
```

Também podemos sobreescrever uma função por outro valor qualquer:

```Javascript
carro.ligar = "22";
carro.ligar = function() {
    //novo código para ligar o carro
}
```

Veremos mais usos de objetos em outras atividades no futuro. Até lá, Haaaappy Hacking!
